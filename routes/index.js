var router = require('express').Router();
var mongoose = require('mongoose');
var Page = require('../models/page');
var Tests = require('../models/tests');
var Site = require('../models/site');

router.get('/', function(req, res, next){
   Page.findOne({isHomePage: true}, function(err, page){
      if (err) { return next(err); }

      if (!page) {
         return next(err);
      }

      Site.findOne({}, function(err, site){
         if(err) return next(err);

         Tests.find({}, function(err, tests){
            if (err) { return next(err); }

            if (!page) {
               return next(err);
            }

            Page.find({}, function(err, pages){
               if (err) { return next(err); }

               if (!pages) {
                  return next(err);
               }

               res.render('index', {
                  page: page,
                  pages: pages,
                  tests: tests,
                  url: req.protocol + '://' + req.hostname + req.originalUrl,
                  site: site
               });
            });
         });

      });

      
   });
});

router.get('/tests/:id', function(req, res, next){
   var url = req.params.id;
   Page.findOne({isHomePage: true}, function(err, homepage){
      if (err) { return next(err); }

      if (!homepage) {
         return next(err);
      }

      Page.findOne({url: url}, function(err, page){
         if (err) { return next(err); }

         if (!page) {
            return next(err);
         }

         Site.findOne({}, function(err, site){
            if(err) return next(err);

            Tests.findOne({_id: mongoose.Types.ObjectId(page.testId)}, function(err, test){
               if (err) { return next(err); }

               if (!test) {
                  return next(err);
               }
               res.render('tests', {
                  test: test,
                  page: page,
                  homepage: homepage,
                  url: req.protocol + '://' + req.hostname + req.originalUrl,
                  site: site
               });
            });
         });
      });
   });
});

module.exports = router;
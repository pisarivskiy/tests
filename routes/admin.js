var router = require('express').Router();
var passport = require('passport');
var gravatar = require('gravatar');
var mongoose = require('mongoose');
var path = require('path');
var User = require('../models/user');
var Page = require('../models/page');
var Test = require('../models/tests');
var Question = require('../models/questions');
var Answer = require('../models/answers');
var Site = require('../models/site');

// Multer setup
var upload  = require('multer')({ dest: './public/uploads' ,
   rename: function (fieldname, filename) {
      return filename+Date.now();
   },
   onFileUploadStart: function (file) {
      console.log(file.originalname + ' is starting ...');
   },
   onFileUploadComplete: function (file) {
      console.log(file.originalname + ' uploaded to  ' + file.path);
   }
});

var env = process.env.NODE_ENV || 'development',
   config = require('../config/config.' + env);

function getAvatarURL(req){
   if (req.isAuthenticated()) {
      return secureUrl = gravatar.url(req.user.email || "", {s: '45', r: 'x', d: 'retro'}, true);
   }
}

router.get('/', function(req, res){
   res.render('admin/index', {
      authorized: req.isAuthenticated(),
      pageUrl: req.headers.host + '/' + config.adminUrl,

      absolutePageUrl: req.protocol + '://' + req.get('host') + req.originalUrl,
      relativePageUrl: req.originalUrl,
      adminPageUrlRel: config.adminUrl,
      adminPageUrlAbs: req.protocol + '://' + req.get('host') + '/' + config.adminUrl,
      homeUrl: req.headers.host,
      
      user: req.user,
      avatar: getAvatarURL(req)
   });
});

router.post('/', function(req, res, next){
   passport.authenticate('login', function(err, user, info){
      if (err) { 
         return next(err);
      }
      if (!user) { 
         req.flash('info', 'Incorrect username or password!');
         req.flash('status', 'error');
         return res.redirect(req.originalUrl); 
      }
      req.logIn(user, function(err) {
         if (err) { 
            return next(err); 
         }
         return res.redirect(req.originalUrl); 
       });
   })(req, res, next);
});

router.get('/user', function(req, res, next){
   if(req.isAuthenticated()){

      Site.findOne({}, function(err, site){
         if(err) return next(err);

         res.render('admin/userSettings', {
            authorized: req.isAuthenticated(),

            absolutePageUrl: req.protocol + '://' + req.get('host') + req.originalUrl,
            relativePageUrl: req.originalUrl,
            adminPageUrlRel: config.adminUrl,
            adminPageUrlAbs: req.protocol + '://' + req.get('host') + '/' + config.adminUrl,
            homeUrl: req.headers.host,

            user: req.user,
            avatar: getAvatarURL(req),
            site: site
         });
      });


   } else {
      res.redirect('/' + config.adminUrl);
   }
});

router.post('/user', function(req, res, next){
   if(req.isAuthenticated()){

      if(req.user.validPassword(req.body.oldpassword)){
         User.findOne({
            username: req.user.username
         }, function(err, doc){
            if(err) { return next(err); }

            var newpass = (req.body.newpassword) ? req.user.generateHash(req.body.newpassword) : undefined;
            doc.username = req.body.username || req.user.username;
            doc.email = req.body.email || req.user.email;
            doc.password = newpass || req.user.password;
            doc.firstname = req.body.firstname || req.user.firstname;
            doc.lastname = req.body.lastname || req.user.lastname;
            doc.save();
         });

         Site.findOne({}, function(err, site){
            if(err) return next(err);
            var s;

            if(site){
               s = site;
            } else {
               s = Site();
            }

            s.title = req.body.title || '';
            s.description = req.body.description || '';
            s.save(function(err){
               if(err) return next(err);
            });
            
         });

         req.flash('info', 'Changes saved!');
         req.flash('status', 'success');
         res.redirect(req.originalUrl); 

      } else {
         req.flash('info', 'Incorrect password!');
         req.flash('status', 'error');
         res.redirect(req.originalUrl); 
      }

   } else {
      res.redirect('/' + config.adminUrl);
   }
});

router.get('/logout', function(req, res){
   req.logout();
   res.redirect('/' + config.adminUrl);
});


/**********************************************
****************** Tests **********************
***********************************************/


router.get('/tests', function(req, res, next){
   if(req.isAuthenticated()){
      Test.find({}, function(err, tests){
         if(err) { return next(err); }

         res.render('admin/testsView', {
            authorized: req.isAuthenticated(),

            absolutePageUrl: req.protocol + '://' + req.get('host') + req.originalUrl,
            relativePageUrl: req.originalUrl,
            adminPageUrlRel: config.adminUrl,
            adminPageUrlAbs: req.protocol + '://' + req.get('host') + '/' + config.adminUrl,
            homeUrl: req.headers.host,

            user: req.user,
            avatar: getAvatarURL(req),
            tests: tests
         });
      });
   } else {
      res.redirect('/' + config.adminUrl);
   }
});

router.get('/tests/create', function(req, res){
   if(req.isAuthenticated()){
      res.render('admin/testsCreate', {
         authorized: req.isAuthenticated(),

         absolutePageUrl: req.protocol + '://' + req.get('host') + req.originalUrl,
         relativePageUrl: req.originalUrl,
         adminPageUrlRel: config.adminUrl,
         adminPageUrlAbs: req.protocol + '://' + req.get('host') + '/' + config.adminUrl,
         homeUrl: req.headers.host,

         user: req.user,
         avatar: getAvatarURL(req)
      });
   } else {
      res.redirect('/' + config.adminUrl);
   }
});

router.get('/tests/edit', function(req, res){
   if(req.isAuthenticated()){
      Test.findOne({_id: mongoose.Types.ObjectId(req.query.id ) }, function(err, test){
         if (err) { return next(err); }

         if (!test) {
            return next(err); 
         }

         res.render('admin/testsEdit', {
            authorized: req.isAuthenticated(),

            absolutePageUrl: req.protocol + '://' + req.get('host') + req.originalUrl,
            relativePageUrl: req.originalUrl,
            adminPageUrlRel: config.adminUrl,
            adminPageUrlAbs: req.protocol + '://' + req.get('host') + '/' + config.adminUrl,
            homeUrl: req.headers.host,

            user: req.user,
            avatar: getAvatarURL(req),
            test: test
         });
         
      });
   } else {
      res.redirect('/' + config.adminUrl);
   }
});

function createTest(req, res, test, parentId){
   /*
      параметры: req, res, test, parentId
      1. определить тип родителя
      2. создать объект
      3. сохранить объект в test
      4. проверить наличие дочерних данных
      5. завершение или вызов для дочерних

   */
   var currentId = null;
   var count = 1;

   // check parrent type
   if(parentId[parentId.length - 3] == 'q'){
      // add answer
      while(true){
         currentId = parentId + '_a_' + count;

         // check if answer exists
         if(req.body['title_' + currentId]) {
            var answer = Answer();
            answer.title = req.body['title_' + currentId] || '';
            test.answers.push(answer);

            // pass to next question
            createTest(req, res, test.answers[test.answers.length - 1], currentId);
         } else {
            break;
         }

         count++;
      }

      return;

   } else {
      // question
      while(true){
         currentId = parentId + '_q_' + count;

         // check if question exists
         if(req.body['title_' + currentId]) {
            var question = Question();
            question.title = req.body['title_' + currentId];

            if(req.files['image_' + currentId]) {
               question.image = req.files['image_' + currentId].path.slice(7) || 0;
            } else if (req.body['imageh_' + currentId]) {
               question.image = req.body['imageh_' + currentId];
            }
            
            question.type = req.body['type_' + currentId] || 0;
            question.description = req.body['description_' + currentId] || '';
            question.share = req.body['share_' + currentId] || '';
            test.next_question = question;

            // pass to next question
            createTest(req, res, test.next_question, currentId);
         } else {
            break;
         }

         count++;
      }

      return;
   }
}

router.post('/tests/edit', function(req, res, next){
   if(req.isAuthenticated()){
      upload(req,res,function(err, file) {
         if(err) {
            return next(err);
         }
         
         Test.findOne({_id: mongoose.Types.ObjectId(req.query.id)}, function(err, test){

            if(err) {
               req.flash('info', 'Error!');
               req.flash('status', 'error');
            }

            test.title = req.body.title;
            test.description = req.body.description;
            test.color = req.body.color;
            if(req.files.image){
               test.image = req.files.image.path.slice(7);
            }

            var parentId = 'q_1';

            if(req.body['title_' + parentId]){

               var question = Question();
               question.title = req.body['title_' + parentId];
               if(req.files['image_' + parentId]) {
                  question.image = req.files['image_' + parentId].path.slice(7) || 0;
               }
               
               question.type = req.body['type_' + parentId] || 0;

               test.question = question || 0;
            }

            //
            createTest(req, res, test.question, parentId);

            test.save(function(err){
               if(err) {
                  req.flash('info', 'Error!');
                  req.flash('status', 'error');
               } else {
                  req.flash('info', 'Saved!');
                  req.flash('status', 'success');
               }
               res.redirect(req.originalUrl); 
            });

         });
         
      });

   } else {
      res.redirect('/' + config.adminUrl);
   }
});

router.post('/tests/create', function(req, res, next){
   if(req.isAuthenticated()){
      upload(req,res,function(err, file) {
         if(err) {
            return next(err);
         }
         
         var test = Test();
         test.title = req.body.title;
         test.description = req.body.description;
         test.color = req.body.color;
         test.image = req.files.image.path.slice(7);

         // 
         var parentId = 'q_1';

         if(req.body['title_' + parentId]){

            var question = Question();
            question.title = req.body['title_' + parentId];
            if(req.files['image_' + parentId]) {
               question.image = req.files['image_' + parentId].path.slice(7) || 0;
            }
            
            question.type = req.body['type_' + parentId] || 0;

            test.question = question || 0;
         }

         //
         createTest(req, res, test.question, parentId);

         test.save(function(err){
            if(err) {
               req.flash('info', 'Error!');
               req.flash('status', 'error');
            } else {
               req.flash('info', 'Saved!');
               req.flash('status', 'success');
            }
            res.redirect(req.originalUrl); 
         });
      });

   } else {
      res.redirect('/' + config.adminUrl);
   }
});

router.get('/tests/delete', function(req, res){
   if(req.isAuthenticated()){
      Test.remove({_id: mongoose.Types.ObjectId(req.query.id ) }, function(err){
         if (err) {
            req.flash('info', 'Error!');
            req.flash('status', 'error');
            res.redirect('/' + config.adminUrl + '/tests'); 
         }
         
         req.flash('info', 'Deleted!');
         req.flash('status', 'success');

         res.redirect('/' + config.adminUrl + '/tests'); 
      }); 

   } else {
      res.redirect('/' + config.adminUrl);
   }
});


/**********************************************
****************** Pages
***********************************************/


router.get('/pages', function(req, res, next){
   if(req.isAuthenticated()){
      Page.find({}, function(err, pages){
         if(err) { return next(err); }
            res.render('admin/pagesView', {
            authorized: req.isAuthenticated(),

            absolutePageUrl: req.protocol + '://' + req.get('host') + req.originalUrl,
            relativePageUrl: req.originalUrl,
            adminPageUrlRel: config.adminUrl,
            adminPageUrlAbs: req.protocol + '://' + req.get('host') + '/' + config.adminUrl,
            homeUrl: req.headers.host,

            user: req.user,
            avatar: getAvatarURL(req),
            pages: pages
         });
      });
      
   } else {
      res.redirect('/' + config.adminUrl);
   }
});

router.get('/pages/create', function(req, res, next){
   if(req.isAuthenticated()){
      Test.find({}, function(err, tests){
         if (err) { return next(err); }

         if (!tests) {
            return next(err);
         }
         res.render('admin/pagesCreate', {
            authorized: req.isAuthenticated(),

            absolutePageUrl: req.protocol + '://' + req.get('host') + req.originalUrl,
            relativePageUrl: req.originalUrl,
            adminPageUrlRel: config.adminUrl,
            adminPageUrlAbs: req.protocol + '://' + req.get('host') + '/' + config.adminUrl,
            homeUrl: req.headers.host,

            user: req.user,
            avatar: getAvatarURL(req),
            tests: tests
         });         
      });
   } else {
      res.redirect('/' + config.adminUrl);
   }
});

router.post('/pages/create', function(req, res){
   if(req.isAuthenticated()){
      var page = Page();

      page.title = req.body.title;
      page.description = req.body.description || page.description;
      page.keywords = req.body.keywords || page.keywords;
      if(req.body.ishomepage == 'on'){
         page.isHomePage = true;
      } else {
         page.isHomePage = false;
      }
      page.url = req.body.link;
      page.type = req.body.type || page.type;

      var testId = null;
      if(req.body.testId && page.isHomePage == false && page.type != 0){
         testId = mongoose.Types.ObjectId(req.body.testId);
         
      }
      page.testId = testId;

      page.save(function(err){
         if(err) {
            req.flash('info', 'Error!');
            req.flash('status', 'error');
         } else {
            req.flash('info', 'Saved!');
            req.flash('status', 'success');
         }
         res.redirect(req.originalUrl); 
      }); 
   } else {
      res.redirect('/' + config.adminUrl);
   }
});

router.get('/pages/edit', function(req, res, next){
   if(req.isAuthenticated()){
      Page.findOne({_id: mongoose.Types.ObjectId(req.query.id ) }, function(err, page){
         if (err) { return next(err); }

         if (!page) {
            return next(err);
         }

         Test.find({}, function(err, tests){
            if (err) { res.send(err); }

            if (!tests) {
               return res.send(err);;
            }

            res.render('admin/pagesEdit', {
               authorized: req.isAuthenticated(),

               absolutePageUrl: req.protocol + '://' + req.get('host') + req.originalUrl,
               relativePageUrl: req.originalUrl,
               adminPageUrlRel: config.adminUrl,
               adminPageUrlAbs: req.protocol + '://' + req.get('host') + '/' + config.adminUrl,
               homeUrl: req.headers.host,

               user: req.user,
               avatar: getAvatarURL(req),
               page: page,
               tests: tests
            });

         });
         
      });

   } else {
      res.redirect('/' + config.adminUrl);
   }
});

router.post('/pages/edit', function(req, res, next){
   if(req.isAuthenticated()){
      Page.findOne({_id: mongoose.Types.ObjectId(req.query.id ) }, function(err, page){
         if (err) { return next(err); }

         if (!page) {
            return next(err);
         }
         page.title = req.body.title;
         page.description = req.body.description || page.description;
         page.keywords = req.body.keywords || page.keywords;
         if(req.body.ishomepage == 'on'){
            page.isHomePage = true;
         } else {
            page.isHomePage = false;
         }
         page.url = req.body.link;
         page.type = req.body.type || page.type;

         var testId = null;
         if(req.body.testId && page.isHomePage == false && page.type != 0){
            testId = mongoose.Types.ObjectId(req.body.testId);
            
         }
         page.testId = testId;

         page.save(function(err){
            if(err) {
               req.flash('info', 'Error!');
               req.flash('status', 'error');
            } else {
               req.flash('info', 'Saved!');
               req.flash('status', 'success');
            }
            res.redirect(req.originalUrl); 
         }); 
      });

   } else {
      res.redirect('/' + config.adminUrl);
   }
});

router.get('/pages/delete', function(req, res, next){
   if(req.isAuthenticated()){
      Page.remove({_id: mongoose.Types.ObjectId(req.query.id ) }, function(err){
         if (err) {
            return next(err);
         }
         
         req.flash('info', 'Deleted!');
         req.flash('status', 'success');

         res.redirect('/' + config.adminUrl + '/pages'); 
      }); 

   } else {
      res.redirect('/' + config.adminUrl);
   }
});

module.exports = router;
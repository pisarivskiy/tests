/**
 * Module dependencies.
 */
var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var flash = require('express-flash');
var uuid = require('node-uuid');

var session = require('express-session');
var passport = require('passport');

var env = process.env.NODE_ENV || 'development',
   config = require('./config/config.' + env);
var mongoose = require('mongoose');
var Page = require('./models/page');
var Statistic = require('./models/statistic');
var Site = require('./models/site');

/**
 * Routes
 */
 
var router = require('./routes/index');
var adminRouter = require('./routes/admin');

var app = express();

mongoose.connect(config.mongo.getURL());

// authorization
require('./passport')(passport);

/**
 * Configure express
 */

// view engine
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('tiny'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(flash());
app.use(session( { 
   secret: config.secret,
   // cookie: {
   //    maxAge: 3600000,
   //    expires: new Date(Date.now() + 3600000)
   // },
   resave: true, 
   saveUninitialized: true
} ));

app.use(express.static(path.join(__dirname, 'public')));

// for passport

app.use(passport.initialize());
app.use(passport.session());

app.use('/', router);
app.use('/' + config.adminUrl, adminRouter);

app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// development error handler
// will print stacktrace
if (config.env === 'development') {
   app.use(function(error, req, res, next) {
      Page.findOne({isHomePage: true}, function(err, page){
          if(err) { return res.send(err.message); }
          
          Site.findOne({}, function(err, site){
            if(err) { return res.send(err.message); }

            res.status(error.status || 500).render('error', {
              page: page,
              message: error.message,
              status: error.status || 500,
              stack: error.stack,
              url: req.protocol + '://' + req.hostname + req.originalUrl,
              site: site
            });
          });
      });
   });
}

// production error handler
// no stacktraces leaked to user
app.use(function(error, req, res, next) {
  Page.findOne({isHomePage: true}, function(err, page){
      if(err) { return res.send(err.message); }
      
      Site.findOne({}, function(err, site){
        if(err) { return res.send(err.message); }

        res.status(error.status || 500).render('error', {
          page: page,
          message: error.message,
          status: error.status || 500,
          stack: '',
          url: req.protocol + '://' + req.hostname + req.originalUrl,
          site: site
        });
      });
  });
});

module.exports = app;
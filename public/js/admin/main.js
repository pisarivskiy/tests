$(document).foundation();
$(document).ready(function(){

   var createTestForm = $('#createTestForm'),
      createTestFormTitle = $('#title'),
      createTestFormLink = $('#link');

   createTestFormTitle.on('change', function(){
      var link = $(this).val().toLowerCase().replace(/ /g, '-');
      createTestFormLink.val(link);
   });

   $('#editLink').on('click', function(){
      createTestFormLink.removeAttr('disabled');
   });

   function readURL(input, _this) {
      if (input.files && input.files[0]) {
         var reader = new FileReader();

         reader.onload = function (e) {
            _this.parent().find('.image-preview img').attr('src', e.target.result);
            console.log(e.target.result);
         }

         console.log(input.files[0]);
         reader.readAsDataURL(input.files[0]);
       }
   }

   $(".js-image").change(function(){
      readURL(this, $(this));
      $(this).parent().find('.image-preview').removeClass('hidden');
   });

   $('.js-delete').on('click', function(e){
      if (!confirm("Are you sure?")) {
         e.preventDefault();
         e.stopPropagation();
      }
   });

   function addQuestionAndAnswer(e){
      /*
         //получаем родителя
         //id родителя
         //количество потомков
         //генерация именю нового в/о
         //что добавляем: вопрос или ответ?
         //добавление в/о
      */

      var parent = $(this).parent();
      var parentId = parent.attr('id');
      var type = null; /* answer - true */
      var childrenCounter = 0;
      var t = null; /* temp */
      var newId = null;
      var newElement = null;

      /* check type */
      if(parent.hasClass('js-answer')){
         type = true;
      } else {
         type = false;
      }

      console.log('type: ' + type);
      
      if(type){
         t = parent.find('> .js-question').length;
         
      } else {
         t = parent.find('> .js-answer').length;
      }
      console.log('t: ' + t);
      childrenCounter = ++t;

      console.log('childrenCounter: ' + childrenCounter);

      if(type){
         newId = parentId + '_q_' + childrenCounter;
      } else {
         newId = parentId + '_a_' + childrenCounter;
      }

      console.log('newId: ' + newId);

      /* add new element */
      if(type){
         newElement = $('.js-question.hidden').clone();
      } else {
         newElement = $('.js-answer.hidden').clone();
      }
      
      newElement.removeClass('hidden');
      newElement.attr('id', newId);

      newElement.find('label[for="title_"]').attr('for', 'title_' + newId);
      newElement.find('#title_').attr('id', 'title_' + newId).attr('name', 'title_' + newId);

      if(type){
         newElement.find('label[for="type_"]').attr('for', 'type_' + newId);
         newElement.find('#type_').attr('id', 'type_' + newId).attr('name', 'type_' + newId);

         newElement.find('label[for="image_"]').attr('for', 'image_' + newId);
         newElement.find('#image_').attr('id', 'image_' + newId).attr('name', 'image_' + newId);

         newElement.find('label[for="description_"]').attr('for', 'description_' + newId);
         newElement.find('#description_').attr('id', 'description_' + newId).attr('name', 'description_' + newId);

         newElement.find('label[for="share_"]').attr('for', 'share_' + newId);
         newElement.find('#share_').attr('id', 'share_' + newId).attr('name', 'share_' + newId);
      }

      parent.append(newElement);

      $('.js-add-answer, .js-add-question').unbind('click', addQuestionAndAnswer);
      $('.js-add-answer, .js-add-question').on('click', addQuestionAndAnswer);

      $('.js-delete-in-test').unbind('click', deleteElement);
      $('.js-delete-in-test').on('click', deleteElement);

      return false;
   }

   $('.js-add-answer, .js-add-question').on('click', addQuestionAndAnswer);

   /* remove elements */
   function deleteElement(e){
      e.preventDefault();
      e.stopPropagation();

      var thisElement = $(this).parent().parent();
      var parentElement = thisElement.parent();
      var id = thisElement.attr('id');
      var type = id[id.length - 3];
      var elements, count = 1;

      thisElement.remove();

      if(type == 'a'){
         elements = parentElement.find('> .js-answer');

         elements.each(function(index){
            var oldId = $(this).attr('id');
            var newId = oldId.slice(0, oldId.length - 2) + '_' + count;
            
            $(this).attr('id', newId);

            var labels = $(this).find('label'),
            inputs = $(this).find('input'), temp;

            labels.each(function(index){
               temp = $(this).attr('for');
               temp = temp.slice(0, temp.length - 2) + '_' + count;
               
               $(this).attr('for', temp);
            });

            inputs.each(function(index){
               temp = $(this).attr('id');
               temp = temp.slice(0, temp.length - 2) + '_' + count;
               
               $(this).attr('id', temp);               
               $(this).attr('name', temp);
            });

            count++;
         });
      } else {
         elements = parentElement.find('> .js-question');

         elements.each(function(index){
            var oldId = $(this).attr('id');
            var newId = oldId.slice(0, oldId.length - 2) + '_' + count;
            
            $(this).attr('id', newId);

            var labels = $(this).find('label'),
            inputs = $(this).find('input'),
            file = $(this).find('file'),
            textareas = $(this).find('textarea'),
            select = $(this).find('select'), temp;

            labels.each(function(index){
               temp = $(this).attr('for');
               temp = temp.slice(0, temp.length - 2) + '_' + count;
               
               $(this).attr('for', temp);
            });

            inputs.each(function(index){
               temp = $(this).attr('id');
               temp = temp.slice(0, temp.length - 2) + '_' + count;
               
               $(this).attr('id', temp);               
               $(this).attr('name', temp);
            });

            textareas.each(function(index){
               temp = $(this).attr('id');
               temp = temp.slice(0, temp.length - 2) + '_' + count;
               
               $(this).attr('id', temp);               
               $(this).attr('name', temp);
            });

            file.each(function(index){
               temp = $(this).attr('id');
               temp = temp.slice(0, temp.length - 2) + '_' + count;
               
               $(this).attr('id', temp);               
               $(this).attr('name', temp);
            });

            select.each(function(index){
               temp = $(this).attr('id');
               temp = temp.slice(0, temp.length - 2) + '_' + count;
               
               $(this).attr('id', temp);               
               $(this).attr('name', temp);
            });

            count++;
         });
      }
   }

   $('.js-delete-in-test').on('click', deleteElement);

   tinymce.init({
      selector: ".tinymce"
   });
});
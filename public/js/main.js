$(document).ready(function(){
   var main = $('main.content.home');

   /* set the right height */
   function setTestHeight(){
      main.height($('body').height() - $('.site-header').outerHeight());
   }

   setTestHeight();

   $(window).on('resize', function(){
      setTestHeight();
   });

   /* tests */
   $('.question__answer').on('click', function(e){
      e.preventDefault();
      e.stopPropagation();

      var parent = $(this).parent();
      var id = $(this).attr('href');

      parent.addClass('question__slide-top');
      setTimeout(function(){
         parent.addClass('question__hidden');
         var nextEl = $(id);
         nextEl.removeClass('question__hidden');
         
         console.log(nextEl.offset().top);

         if(nextEl.hasClass('question--final')){
            $('html, body').animate({
               scrollTop: nextEl.offset().top
            }, 1500, 'swing');
         }

      }, 500);
   });
});

$(window).load(function(){
   setTimeout(function(){
      $('.preloader').addClass('preloader--fade-out');
   }, 1500);

   setTimeout(function(){
      $('.preloader').removeClass('preloader--visible');
   }, 2000);
});
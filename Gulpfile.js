var gulp = require('gulp'),
    compass = require('gulp-compass'),
    uglifyjs = require('gulp-uglifyjs'),
    concat = require('gulp-concat');

var paths = {
    dist: {
        distDir: './',
        cssDir: './public/css/',
        jsDir: './public/js'
    },
    src: {
        sassDir: './src/scss/',
        sassFilter: ['./src/scss/**/*.scss', './src/scss/*.scss'],
        jsFilter: ['./src/js/libs/*.js', './src/js/*.js']
    }
};

/* Compile sass */
gulp.task('compile-sass', function(){
    gulp.src(paths.src.sassFilter)
        .pipe(compass({
            config_file: 'config.rb',
            css: paths.dist.cssDir,
            sass: paths.src.sassDir
        }))
        .pipe(gulp.dest(paths.dist.cssDir));

});

/* JS minification and concatenation */
gulp.task('js', function(){
    gulp.src(paths.src.jsFilter)
        .pipe(uglifyjs())
        .pipe(concat('all.js'))
        .pipe(gulp.dest(paths.dist.jsDir));
});

/* Just copy html to dist directory */
gulp.task('copy-html', function(){
    gulp.src(paths.src.htmlFilter)
        .pipe(gulp.dest(paths.dist.distDir));
});



/* Watchers */
gulp.task('watch', function(){
    gulp.watch(paths.src.sassFilter, ['compile-sass']);
    gulp.watch(paths.src.jsFilter, ['js']);
});

gulp.task('default', ['watch']);
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var answerSchema = mongoose.Schema({
   title: String,
   next_question: Object
});

module.exports = mongoose.model('answers', answerSchema);
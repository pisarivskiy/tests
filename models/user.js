var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');

var userSchema = mongoose.Schema({
   username: String,
   password: String,
   email: String,
   firstname: String,
   lastname: String,
   avatar: String
});

userSchema.methods.validPassword = function(password){
   return bcrypt.compareSync(password, this.password);
};

userSchema.methods.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

module.exports = mongoose.model('users', userSchema);
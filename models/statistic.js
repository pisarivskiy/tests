var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var staticticSchema = mongoose.Schema({
   uuid: String,
   lastVisit: String,
   complitedTests: Array
});

module.exports = mongoose.model('statistic', staticticSchema);
var mongoose = require('mongoose');

var pageSchema = mongoose.Schema({
   isHomePage: Boolean,
   url: String,
   type: Number,
   title: String,
   description: String,
   keywords: String,
   testId: mongoose.Schema.Types.ObjectId
});

// userSchema.methods.validPassword = function(password){
//    return bcrypt.compareSync(password, this.password);
// };

// userSchema.methods.generateHash = function(password) {
//     return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
// };

module.exports = mongoose.model('pages', pageSchema);
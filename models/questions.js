var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var questionSchema = mongoose.Schema({
   title: String,
   description: String,
   share: String,
   image: String,
   type: Number,
   answers: Array
});

module.exports = mongoose.model('questions', questionSchema);
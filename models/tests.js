var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var testSchema = mongoose.Schema({
   title: String,
   description: String,
   image: String,
   color: String,
   question: Object
});

module.exports = mongoose.model('tests', testSchema);
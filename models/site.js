var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var siteSchema = mongoose.Schema({
   title: String,
   description: String
});

module.exports = mongoose.model('sites', siteSchema);